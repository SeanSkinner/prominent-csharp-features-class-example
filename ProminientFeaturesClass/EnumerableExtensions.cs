﻿namespace ProminientFeaturesClass
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Filter<T>(this IEnumerable<T> source, Func<T,bool> predicate)
        {
            if (source is null)
            {
                throw new NullReferenceException("source sequence was null");
            }

            var enumerator = source.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var current = enumerator.Current;

                if (predicate.Invoke(current))
                {
                    yield return current;
                }
            }
        }

        public static IEnumerable<T> Filter<T>(this IEnumerable<T> source, Func<T, bool> predicate, Func<T, bool> predicateTwo)
        {
            if (source is null)
            {
                throw new NullReferenceException("source sequence was null");
            }

            var enumerator = source.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var current = enumerator.Current;

                if (predicate.Invoke(current) && predicateTwo.Invoke(current))
                {
                    yield return current;
                }
            }
        }
    }
}