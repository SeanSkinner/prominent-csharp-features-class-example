﻿using ProminientFeaturesClass;

var input = new int[] { 1, 2, 3, 4, 5 };
var inputIntersect = new int[] {1, 4, 5 };
var unionInput = new int[] { 1, 2, 6, 7 };
var duplicateInput = new int[] { 1, 1, 1, 2, 2, 2, 3 };

//filter
var filter = input.Where(x => x > 2);
var filterCustom = input.Filter(x => x > 2);
//map
var select = input.Select(x => x * 2);
//reduce
var reduce = input.Aggregate(10, (acc, curr) => acc + curr);

var reduce2 = input.Aggregate(10, (acc, curr) => acc + curr, (result) => (double) result);
//first (or default)
var first = input.FirstOrDefault(x => x > 10);
//single (or default)
var single = input.SingleOrDefault(x => x == 6);
//all
var all = input.All(x => x < 6);
//any
var any = input.Any(x => x < 6);
//intersects
var intersects = input.Intersect(inputIntersect);
//cast
var cast = input.Cast<double>();
//append
var appended = input.Append(999);
//union
var union = input.Union(unionInput);
//distinct
var distinct = duplicateInput.Distinct();
//groupby
var grouped = duplicateInput.GroupBy(
    x => x,
    x => x,
    (key, value) => new { Key = key, Count = value.Count() }
);

//converting IEnumerable to a specific, concrete collection
var selectAsList = select.ToList();
var selectAsArray = select.ToArray();


//converting a specific collection back to a general IEnumerable
var selectBackToEnumerable = selectAsList.AsEnumerable();

var filterAndMap = input
    .Where(x => x > 2)
    .Select(x => x * 2);

Console.WriteLine();